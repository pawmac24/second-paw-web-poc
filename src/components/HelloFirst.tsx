import React, {Component} from "react";

export interface HelloProps {
    compiler: string;
    framework: string;
}

//stateless functional component
//export const HelloFirst = (props: HelloProps) => <h1>Hello from {props.compiler} and ccc {props.framework}!</h1>;

// 'HelloProps' describes the shape of props.
// State is never set so we use the '{}' type.
export class HelloFirst extends Component<HelloProps, {}> {
    render() {
        return <h1>Hello from {this.props.compiler} and {this.props.framework}!</h1>;
    }
}
